
 //Теоретичне питання
 // Ajax - це технологія, за допомогою якої можна отримати (відправити) дані з серверу для подальшого використання без перезавантаження сторінки.
 // Перевагою Ajax є економія трафіку (сайт завантажується швидше), сайт стає більш зручним і гучним у використанні.

 function loadInformationOfFilm(){
   fetch('https://ajax.test-danit.com/api/swapi/films').then(res => res.json()).then( async episodes => {


       for (const episode of episodes) {
               let div = document.createElement('div')
               div.className = "episode"
           div.innerHTML = `
                        <h1 class="episode_name"> Episode №${episode.episodeId}: ${episode.name}</h1>
                            <p class="episode_description"> ${episode.openingCrawl}</p>
                      <h2>The list of characters that were shown in this film.</h2>
                      <ol class="characters">
                      </ol>
                `

           const ol = div.querySelector(".characters")

           for (const actorApi of episode.characters) {
               const response = await fetch(actorApi)
               const actor = await response.json()
               const li = document.createElement("li")
               li.innerText = actor.name
                ol.append(li)
           }
           document.body.append(div)

       }
   })
}
 loadInformationOfFilm()


