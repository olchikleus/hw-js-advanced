
const usersUrl = "https://ajax.test-danit.com/api/json/users";
const postsUrl = "https://ajax.test-danit.com/api/json/posts";

const ul = document.querySelector(".cards__information")


class Card {
    constructor(id, userId,title,body) {
        this._title = title;
        this._body = body;
        this._userId = userId;
        this._postId = id;
    }

    async renderCard() {
        let getUsersInformation = await fetch(usersUrl);
        let usersInformation = await getUsersInformation.json();
        usersInformation.forEach((information) => {
            if (this._userId === information.id) {
                ul.innerHTML += `
                             <li class="post post__${this._postId}">
                                <i class="fa-solid fa-trash-can" data-id = ${this._postId} ></i>
                                <p class="post__author"> ${information.name} <span>${information.email}</span> </p>
                                <h1 class="post__title">"${this._title}"</h1>
                                <p class="post__body">${this._body}</p>
                             </li>
                           `
            }
        });
    }
}

async function getPostInformation() {
    let getPostInformation = await fetch(postsUrl);
    let postInformation = await getPostInformation.json();
    postInformation.forEach((post) => {
        let card = new Card(post.id, post.userId, post.title, post.body);
        card.renderCard();
    });
}

getPostInformation();


document.querySelector(".cards__information").addEventListener("click", async (e) => {
    if (e.target.classList.contains("fa-solid")) {
        const postId = e.target.getAttribute("data-id")
        await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
            method: 'DELETE',
        }).then((response) => {
            document.querySelector(`li`).remove();
            console.log(response)
        });
    }
});


