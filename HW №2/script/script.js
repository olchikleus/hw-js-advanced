const books = [
    {author: "Люсі Фолі", name: "Список запрошених", price: 70},
    {author: "Сюзанна Кларк", name: "Джонатан Стрейндж і м-р Норрелл"},
    {name: "Дизайн. Книга для недизайнерів.", price: 70},
    {author: "Алан Мур", name: "Неономікон", price: 70},
    {author: "Террі Пратчетт", name: "Рухомі картинки", price: 40},
    {author: "Анґус Гайленд", name: "Коти в мистецтві"},
];

// console.log(books)

let div = document.createElement("div");
div.setAttribute("id", "root")
document.body.append(div)

function createList (array, element){
    const ul = document.createElement('ul');
    element.append(ul)

    books.forEach(element =>{

            try {
                if (!element.hasOwnProperty('name')) {
                    throw (new Error(`${JSON.stringify(element)} Об'єкт не має властивості "Назва книги"`))
                }

                if (!element.hasOwnProperty('author')) {
                    throw (new Error(`${JSON.stringify(element)} Об'єкт не має властивості "Автор"`))
                }

                if (!element.hasOwnProperty('price')) {
                    throw (new Error(`${JSON.stringify(element)} Об'єкт не має властивості "Ціна"`))
                }

                let li = document.createElement('li');
                li.textContent = `Автор:${element.author} , назва книги:"${element.name}", ціна:${element.price}грн`
                ul.append(li);
            }
            catch (e) {
                console.log (e.message)
            }
    })
}

createList (books, div)


// let div = document.createElement("div");
// div.setAttribute("id", "root")
// document.body.append(div)
//
// function createList (array, element){
//     const ul = document.createElement('ul');
//     element.append(ul)
//
//     books.map (element => {
//         try {
//             if (!element.hasOwnProperty('name')) {
//                 throw (new Error(`${JSON.stringify(element)} Об'єкт не має властивості "Назва книги"`))
//             }
//
//             if (!element.hasOwnProperty('author')) {
//                 throw (new Error(`${JSON.stringify(element)} Об'єкт не має властивості "Автор"`))
//             }
//
//             if (!element.hasOwnProperty('price')) {
//                 throw (new Error(`${JSON.stringify(element)} Об'єкт не має властивості "Ціна"`))
//             }
//
//             let li = document.createElement('li');
//             li.textContent = `Автор:${element.author} , назва книги:"${element.name}", ціна:${element.price}грн`
//             ul.append(li);
//         }
//
//         catch (e) {
//             console.log (e.message)
//         }
//     })
//
// }
// createList (books, div)
