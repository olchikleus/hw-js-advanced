<!--Теоретичне питання-->

<!--1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript-->
<!--Наприклад, у нас є багато  однотипниx об'єктів, властивості і методи яких повторюються. -->
<!--Ми можемо створити  прототип, заначивши в ньому всі властивості і методи. -->
<!--Далі ми створюємо на основі прототипу об'єкти. -->
<!--За замовчуванням, у створених об'єктів вже буде унаслідовано методи і властивості протопипу. -->
<!--Якщо якийсь метод або властивість у об'єкта потрібно змінити, ми вже в ньому зазначаємо це явно.-->

<!--2. Для чого потрібно викликати super() у конструкторі класу-нащадка?-->
<!--В конструкторах класу-нащадка потрібно викликати super з усіма аргументами батьківського класу, для того щоб вподальшому ми мали змогу використовувати this.-->



    class Employee {
    constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
};

    set name (name){
    this._name = name
};
    get name (){
    return this._name
};

    set age (age){
    this._age = age
};
    get age (){
    return this._age
};
    set salary (salary){
    this._salary = salary
};
    get salary (){
    return this._salary
};
}


    class  Programmer extends Employee{
    constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang= lang
};
    get salary(){
    return this._salary * 3
}
}
    const manager = new Programmer("Olga", 28, 20000, "UK")
    const programmer = new Programmer("Vasya", 22, 10000, "ENG")

    console.log(manager, programmer)
    console.log(manager.salary, programmer.salary)


