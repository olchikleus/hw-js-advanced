

const btn  = document.createElement("button")
btn.innerText = "Find by IP"
document.body.append(btn)

btn.addEventListener("click", async function getIP(){
    const responseIP = await fetch('https://api.ipify.org/?format=json')
    const {ip} = await responseIP.json()

    const responseInform = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,region,city,district`)
    const inform = await responseInform.json()
         const div = document.querySelector(".information")
         if(inform.district === ""){
             inform.district = 'Information not found'
         }
         div.innerHTML =
             `
             <div class="information">
             <p class="query"> Query: ${ip} </p>
                <p class="continent"> Continent: ${inform .continent} </p>
                <p class="country"> Country: ${inform .country}</p>
                <p class="region"> region: ${inform .region}</p>
                <p class="city"> City: ${inform .city}</p>
                <p class="district"> District: ${inform .district}</p>
            </div>
             `

})




